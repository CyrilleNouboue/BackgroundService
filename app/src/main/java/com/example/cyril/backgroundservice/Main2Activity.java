package com.example.cyril.backgroundservice;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

public class Main2Activity extends AppCompatActivity {

    private Button start, stop; // Bouttons de controle du service

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        // Bouttons de controle du service

        /*
        start = (Button) findViewById(R.layout.activity_main2);
        stop =(Button) findViewById(R.layout.activity_main2);

        start.setOnClickListener(this);
        stop.setOnClickListener(this);
        */

        //On lance le service dès l'ouverture del'activité
        startService(new Intent(getApplicationContext(),Service2.class));

    }



}
